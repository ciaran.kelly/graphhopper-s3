ARG AWS_CLI_VERSION=1.18.127
ARG PYTHON_MAJOR_VERSION=3.7
ARG DEBIAN_VERSION=buster-20200803-slim

FROM debian:${DEBIAN_VERSION} as aws-cli
ARG AWS_CLI_VERSION
ARG PYTHON_MAJOR_VERSION
RUN apt-get update && \
    apt-get install -y --no-install-recommends python3=${PYTHON_MAJOR_VERSION}.3-1 && \
    apt-get install -y --no-install-recommends python3-pip=18.1-5 && \
    pip3 install setuptools==49.6.0 && \
    pip3 install awscli==${AWS_CLI_VERSION}

FROM graphhopper/graphhopper:latest
ARG PYTHON_MAJOR_VERSION
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    ca-certificates=20190110 \
    git=1:2.20.1-2+deb10u3 \
    jq=1.5+dfsg-2+b1 \
    python3=${PYTHON_MAJOR_VERSION}.3-1 \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
  && update-alternatives --install /usr/bin/python python /usr/bin/python${PYTHON_MAJOR_VERSION} 1
COPY --from=aws-cli /usr/local/bin/aws* /usr/local/bin/
COPY --from=aws-cli /usr/local/lib/python${PYTHON_MAJOR_VERSION}/dist-packages /usr/local/lib/python${PYTHON_MAJOR_VERSION}/dist-packages
COPY --from=aws-cli /usr/lib/python3/dist-packages /usr/lib/python3/dist-packages

ENV JAVA_OPTS "-Xmx5g -Xms5g"

# https://download.geofabrik.de/europe/andorra-latest.osm.pbf
ENV MAPFILE ""

# s3://mybucket/e1ed01c9ed6735b7e083ba8fb2f4f777/europe-andorra-20200917235148.tar.gz
ENV S3MAPCACHE ""
ENV BUCKETNAME ""
ENV AWS_ACCESS_KEY_ID ""
ENV AWS_SECRET_ACCESS_KEY ""
ENV MINSTORAGE ""
ENV GH_OPTIONS ""
COPY . .
ENTRYPOINT [ "./download_import_save.sh" ]
