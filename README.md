This docker container is a combination of cache-warmer and graphhopper server.

# Usage

## Cache Warming
The following will:
 - download a copy of `MAPFILE`
 - start up graphhopper, using a pre-baked yml config (`import-config.yml`)
 - wait for the map to be imported
 - create an archive file of the cached map
 - upload the archive to `BUCKETNAME`

```bash
$ docker run --rm -it \
             -e AWS_ACCESS_KEY_ID="XXXX" \
             -e AWS_SECRET_ACCESS_KEY="XXXX" \
             -e BUCKETNAME=floconcepts-graphhopper-cache \
             -e MAPFILE="https://download.geofabrik.de/europe/faroe-islands-latest.osm.pbf" \
             registry.gitlab.com/ammeon-carrymorecars/infrastructure/graphhopper-s3:latest
```

## GraphHopper Serving
The following will:
 - download `S3MAPCACHE` from the given s3 URL
 - extract the map cache
 - run the GraphHopper webserver using the extracted cache
 
```bash
$ docker run --rm -it \
             -e AWS_ACCESS_KEY_ID="XXXX" \
             -e AWS_SECRET_ACCESS_KEY="XXX" \
             -e S3MAPCACHE=s3://floconcepts-graphhopper-cache/0361471d8f1a733abbd327fda4ea7565/europe-britain-and-ireland-20200922160416.tar.gz \
             -p 80:8989 \
             registry.gitlab.com/ammeon-carrymorecars/infrastructure/graphhopper-s3:latest
```