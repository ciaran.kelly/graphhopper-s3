#! /bin/bash

# FAIL on error...
set -euf -o pipefail
STORAGE="/graphhopper-storage"
mkdir -p ${STORAGE}

MAPSLUG=$(echo ${MAPFILE} | sed "s@https\?://download.geofabrik.de/\(.*\)-latest\(.*\)@\1-$(date '+%Y%m%d%H%M%S')\2@g" | tr '/' '-')
LOCALMAP=${STORAGE}/${MAPSLUG}

log() {
    m_time=`date "+%F %T"`
    echo "${m_time} ${1}"
}

error() {
  m_time=`date "+%F %T"`
  >&2 echo "${m_time} ${1}"
}

verify_storage_available() {
  if [ ! -z ${MINSTORAGE} ]; then
    log "Verifying that at least ${MINSTORAGE}MB storage is available"

    dd if=/dev/zero of=${STORAGE}/zero_test_file bs=1M count="${MINSTORAGE}"
    rm -f ${STORAGE}/zero_test_file
    log "Storage verified"
  fi
}


verify_aws_credentials() {
  IDENTITY=$(aws sts get-caller-identity 2>/dev/null)
  if [ ! -z "${IDENTITY}" ]; then
    log "Credentials OK!"
    log "${IDENTITY}"
    return 0
  else
    error "Could not verify AWS credentials"
    exit 1
  fi
}

verify_s3_bucket() {
  log "Checking for s3://${BUCKETNAME}"
  if aws s3 ls ${BUCKETNAME} > /dev/null; then
    log "Found s3://${BUCKETNAME}!"
    return 0
  else
    error "Could not find s3://${BUCKETNAME}!"
    exit 1
  fi
}

get_map_md5() {
  log "Fetching MD5 of ${MAPFILE}"
  wget "${MAPFILE}.md5" -O  "${LOCALMAP}.md5" --quiet
  if [ "$?" -ne 0 ]; then
    error "Error during MD5 download"
    exit 1
  else
    log "${MAPFILE}.md5 downloaded"
    return 0
  fi
}

check_map_already_processed() {
  get_map_md5
  MAPMD5=$(cat ${LOCALMAP}.md5 | awk '{print $1}')
  S3PATH="s3://${BUCKETNAME}/${MAPMD5}"
  log "Checking : ${S3PATH}/"
  PROCESSED=$(aws s3 ls ${S3PATH}/)
  if [ $? -eq 0 ]; then
    PROCESSED=$(echo ${PROCESSED} | awk '{print $NF}')
    error "Looks like we have already processed this map: ${S3PATH}/${PROCESSED}"
    exit 0
  else
    log "Looks like we havn't processed this map before"
    return 0
  fi
}

download_map() {
  log "Downloading ${MAPFILE}"
  wget ${MAPFILE} -O  ${LOCALMAP} --quiet
  if [ "$?" -ne 0 ]; then
    error "Error during download"
    exit 1
  else
    log "${MAPFILE} downloaded"
    log "$(ls -alh ${LOCALMAP})"
    return 0
  fi
}

build_cache() {
  log "Building cache for ${MAPFILE}"
  GHVERSION=$(./graphhopper.sh --version)
  log "GRAPHHOPPER VERSION: ${GHVERSION}"
  JAVA_OPTS="${JAVA_OPTS} ${GH_OPTIONS}"
  ./graphhopper.sh -a import -i ${LOCALMAP} -c import-config.yml 2>&1 | tee ${STORAGE}/graphhopper_cache.log
  return 0
}

archive_cache() {
  FILENAME=${1}
  log "Creating archive of GraphHopper Cache"
  tar -czf ${FILENAME} -C ${STORAGE} .
  log "${FILENAME} created."
  CONTENTS="$(tar -tvf ${FILENAME})"
  log "Archive Contents: ${CONTENTS}"
}

store_archive() {
  MAPMD5=$(cat ${LOCALMAP}.md5 | awk '{print $1}')
  rm -f ${LOCALMAP}.md5
  ARCHIVEFILE="$(basename $LOCALMAP .osm.pbf).tar.gz"
  S3KEY="${MAPMD5}/${ARCHIVEFILE}"
  S3PATH="s3://${BUCKETNAME}/${S3KEY}"
  cp import-config.yml ${STORAGE}/config.yml
  archive_cache ${ARCHIVEFILE}
  log "Uploading to S3 (${S3PATH})"
  aws s3 cp ${ARCHIVEFILE} ${S3PATH}
  log "Uploaded to S3"
  aws s3 ls ${S3PATH}
}


download_map_from_s3_and_serve() {
  log "Downloading ${S3MAPCACHE} to ${STORAGE}"
  aws s3 cp ${S3MAPCACHE} ${STORAGE}
  log "Downloaded ${S3MAPCACHE}, now extracting"
  tar -xzf ${STORAGE}/$(basename ${S3MAPCACHE}) -C ${STORAGE}
  log "Extracted cached files"
  rm -rf ${STORAGE}/$(basename ${S3MAPCACHE})
  THEMAP=$(find ${STORAGE} -name "*.osm.pbf")
  log "Detected ${THEMAP} to serve"
  ls -al ${STORAGE}
  JAVA_OPTS="${JAVA_OPTS} -Ddw.server.application_connectors[0].bind_host=0.0.0.0 -Ddw.server.application_connectors[0].port=8989"
  ./graphhopper.sh -a web -i ${THEMAP} -c ${STORAGE}/config.yml
}

verify_storage_available

if [ -z "${S3MAPCACHE}" ];
then
  if verify_aws_credentials; then
    if verify_s3_bucket; then
      if check_map_already_processed; then
        if download_map; then
          if build_cache; then
            store_archive
          fi
        fi
      fi
    fi
fi
else
  if verify_aws_credentials; then
    download_map_from_s3_and_serve
  fi
fi

log "Finished"